﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

using Newtonsoft.Json;
using Serilog;
using apexchat.Extensions;
using System.Collections.Generic;

namespace apexchat.FIO
{
    public class Request
    {
        public static readonly HttpClient httpClient;
        public static string RootUrl;

        static Request()
        {
            HttpClientHandler handler = new HttpClientHandler()
            {
                AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
            };
            httpClient = new HttpClient(handler);
            httpClient.Timeout = TimeSpan.FromSeconds(300.0);
        }

        public HttpStatusCode StatusCode
        {
            get;
            private set;
        } = HttpStatusCode.UnavailableForLegalReasons;

        public string ResponsePayload
        {
            get;
            private set;
        }

        private HttpMethod Method = HttpMethod.Get;
        private string EndPoint = null;
        private Dictionary<string, string> Payload = null;
        private string ContentType = null;

        public Request(string Method, string EndPoint, Dictionary<string, string> Payload = null, string ContentType = null)
        {
            if (Method.ToUpper() == "GET")
                this.Method = HttpMethod.Get;
            if (Method.ToUpper() == "POST")
                this.Method = HttpMethod.Post;
            if (Method.ToUpper() == "PUT")
                this.Method = HttpMethod.Put;
            if (Method.ToUpper() == "DELETE")
                this.Method = HttpMethod.Delete;
            if (Method.ToUpper() == "HEAD")
                this.Method = HttpMethod.Head;
            if (Method.ToUpper() == "OPTIONS")
                this.Method = HttpMethod.Options;
            if (Method.ToUpper() == "PATCH")
                this.Method = HttpMethod.Patch;
            this.EndPoint = EndPoint;
            this.Payload = Payload;
            if (ContentType != null)
            {
                this.ContentType = ContentType;
            }
        }

        public Request(HttpMethod Method, string EndPoint, Dictionary<string, string> Payload = null, string ContentType = null)
        {
            this.Method = Method;
            this.EndPoint = EndPoint;
            this.Payload = Payload;
            if (ContentType != null)
            {
                this.ContentType = ContentType;
            }
        }

        public async Task<JsonRepr> GetResponseAsync<JsonRepr>()
        {
            return await GetResponseAsync<JsonRepr>(CancellationToken.None);
        }

        // TODO: Fix this; payload changed from string to dictionary.

        public async Task<JsonRepr> GetResponseAsync<JsonRepr>(CancellationToken ct)
        {
            try
            {
                string result = await GetResultAsStringAsync(ct);
                return JsonConvert.DeserializeObject<JsonRepr>(result);
            }
            catch (JsonException ex)
            {
                Console.Write($"Error deserializing json: {ex.Message}\n");
            }
            catch
            {
                Log.Error($"Unknown error deserializing json.\n");
            }

            return default(JsonRepr);
        }

        public async Task<string> GetResultAsStringAsync()
        {
            return await GetResultAsStringAsync(CancellationToken.None);
        }

        public async Task<string> GetResultAsStringAsync(CancellationToken ct)
        {
            StatusCode = HttpStatusCode.UnavailableForLegalReasons;
            ResponsePayload = null;

            try
            {
                Uri path = new Uri(RootUrl + EndPoint);
                if (Method == HttpMethod.Get && Payload != null)
                {
                    // Payload is treated as a query string for Get requests.
                    foreach (var entry in Payload)
                    {
                        path = path.AddQuery(entry.Key, entry.Value);
                    }
                }
                Log.Verbose("Preparing query: " + path.ToString());

                using (HttpRequestMessage httpRequestMessage = new HttpRequestMessage(Method, path))
                {
                    if (Method != HttpMethod.Get && !String.IsNullOrWhiteSpace(Payload.ToString()))
                    {
                        httpRequestMessage.Content = new StringContent(Payload.ToString(), Encoding.UTF8, ContentType);
                    }

                    using (Task<HttpResponseMessage> queryTask = httpClient.SendAsync(httpRequestMessage, HttpCompletionOption.ResponseContentRead, ct))
                    {
                        HttpResponseMessage response = queryTask.Result;
                        StatusCode = response.StatusCode;
                        ResponsePayload = await response.Content.ReadAsStringAsync(ct);
                    }
                }
            }
            catch (HttpRequestException e)
            {
                if (e.StatusCode != null)
                {
                    StatusCode = (HttpStatusCode)e.StatusCode;
                }
            }
            catch (TaskCanceledException)
            {
                StatusCode = HttpStatusCode.RequestTimeout;
                Log.Verbose("Request timed out.");
            }

            return ResponsePayload;
        }

        public async Task GetResultNoResponse()
        {
            await GetResultNoResponse(CancellationToken.None);
        }

        public async Task GetResultNoResponse(CancellationToken ct)
        {
            StatusCode = HttpStatusCode.UnavailableForLegalReasons;
            ResponsePayload = null;

            try
            {
                using (HttpRequestMessage httpRequestMessage = new HttpRequestMessage(Method, RootUrl + EndPoint))
                {
                    if (!String.IsNullOrWhiteSpace(Payload.ToString()))
                    {
                        httpRequestMessage.Content = new StringContent(Payload.ToString(), Encoding.UTF8, ContentType);
                    }

                    using (HttpResponseMessage response = await httpClient.SendAsync(httpRequestMessage, HttpCompletionOption.ResponseContentRead, ct))
                    {
                        StatusCode = response.StatusCode;
                    }
                }
            }
            catch (HttpRequestException exc)
            {
                if (exc.StatusCode != null)
                {
                    StatusCode = (HttpStatusCode)exc.StatusCode;
                    Log.Debug("HttpRequestException: " + exc.Message);
                }
            }
            catch (TaskCanceledException exc)
            {
                StatusCode = HttpStatusCode.RequestTimeout;
                Log.Warning("GetResultNoResponse task cancelled: " + exc.Message);
            }
        }
    }
}
