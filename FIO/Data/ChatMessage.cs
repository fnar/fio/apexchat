﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace apexchat.FIO.Data
{
    class ChatMessage
    {
        public string MessageId { get; set; }
        public string MessageType { get; set; }
        public string SenderId { get; set; }
        public string UserName { get; set; }
        public string MessageText { get; set; }
        public long MessageTimestamp { get; set; }
        public bool MessageDeleted { get; set; }

        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }

        public string ChatModelId { get; set; } // apex channel id
    }
}
