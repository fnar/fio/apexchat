﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace apexchat.FIO.Data
{
    class Channel
    {
        public string DisplayName { get; set; }
        public string ChannelId { get; set; }
        public string NaturalId { get; set; }
    }
}
