﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Serilog;

namespace apexchat.FIO
{
    class Channels
    {
        static List<FIO.Data.Channel> ChannelList = null;
        static DateTime LastListUpdate = DateTime.Now;

        public static List<FIO.Data.Channel> GetChannelsMatching(string nameOrId)
        {
            FetchChannelListIfNecessary();
            string upperName = nameOrId.ToUpper();
            return ChannelList
                .Where(ch => ch.ChannelId.ToUpper() == upperName || ch.DisplayName.ToUpper().Contains(upperName))
                .ToList();
        }

        static async void FetchChannelListIfNecessary()
        {
            if (ChannelList == null || LastListUpdate.AddMinutes(60) < DateTime.Now)
            {
                Log.Debug("Requesting a fresh channel list.");
                var request = new FIO.Request("Get", "/chat/list");
                ChannelList = await request.GetResponseAsync<List<FIO.Data.Channel>>();
                LastListUpdate = DateTime.Now;
            }
        }
    }
}
