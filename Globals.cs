﻿using System;
using System.IO;

using Microsoft.Extensions.Configuration;
using Serilog;

namespace apexchat
{
    public enum DBTypes
    {
        Sqlite,
        Postgresql,
    }

    public class Globals
    {
        public static ConfigurationRoot config = null;
        public static bool ShouldApplyMigration = false;
        public static bool DebugDatabase = false;
        public static Int32 chatCheckDelayMs = 2000;
        public static uint maxMessageHistoryCount = 500;
        public static DBTypes DatabaseType;

        public static string ConnectionString
        {
            get
            {
                Globals.InitConfiguration();

                var dbconf = config.GetSection("database");
                return SqliteConnectionString(dbconf);
            }
        }

        public static string RootFIOUrl
        {
            get
            {
                Globals.InitConfiguration();
                var dbconf = config.GetSection("FIO");
                return dbconf.GetValue<string>("URL");
            }
        }

        private static string SqliteConnectionString(IConfigurationSection dbconf)
        {
            var str = $"Data Source=\"{dbconf.GetValue<string>("filepath")}\"";
            return str;
        }


        public static void InitConfiguration()
        {
            if (config == null)
            {
                ReloadConfiguration();
            }
        }

        public static void ReloadConfiguration()
        {
            Globals.config = (ConfigurationRoot)new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("DOTNET_ENVIRONMENT") ?? "production"}.json", true)
                .Build();
        }

    }
}