﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.SlashCommands;
using DSharpPlus.SlashCommands.Attributes;
using Microsoft.EntityFrameworkCore;
using Serilog;

namespace apexchat.Commands;

public class ChatSlashCommands : ApplicationCommandModule
{
    [SlashCommand("playback", "Plays back a provided APEX chat channel to this channel.")]
    [SlashRequireUserPermissions(Permissions.ManageChannels)]
    public async Task Playback(InteractionContext ctx,
        [Option("apexChannel", "APEX channel to replay here.")]
        string apexChannel)
    {
        if (ctx.Channel.IsPrivate)
        {
            await ctx.CreateResponseAsync("I cannot play back apex chat directly to you.", true);
            return;
        }
        Log.Debug("Member has permissions to administrate");
        // Very likely to be a long-running operation; tell our sender to wait.
        await ctx.DeferAsync(true);

        var channels = FIO.Channels.GetChannelsMatching(apexChannel);
        Log.Debug($"Got channel list: {channels.Count} matching channels");
        if (channels.Count == 0)
        {
            var response = new DiscordEmbedBuilder();
            response.WithColor(DiscordColor.IndianRed);
            response.Description = $"Unable to find any channels like {apexChannel}";
            await ctx.EditResponseAsync(new DiscordWebhookBuilder().AddEmbed(response));
            return;
        }
        if (channels.Count > 1)
        {
            var response = new DiscordEmbedBuilder();
            response.WithColor(DiscordColor.IndianRed);
            response.Description = $"Found multiple channels matching {apexChannel}.  Be more specific.\n" +
                string.Join(", ", channels.Take(10).Select(c => c.DisplayName));
            await ctx.EditResponseAsync(new DiscordWebhookBuilder().AddEmbed(response));
            return;
        }
        var channel = channels.First();

        try
        {
            using (var db = DB.ChatContext.GetNewContext())
            {
                Log.Debug("Building up the apex/discord chat map");
                var playbackchannel = new DB.Model.ChannelRegistration();
                playbackchannel.ApexChannelId = channel.ChannelId;
                playbackchannel.DiscordChannelID = ctx.Channel.Id;
                playbackchannel.ApexChannelName = channel.DisplayName;
                Log.Debug("Building up the apex/discord chat map (2)");
                db.ChannelRegistrations.Add(playbackchannel);
                Log.Debug("Building up the apex/discord chat map (3)");
                db.SaveChanges();
                Log.Debug("Building up the apex/discord chat map (complete)");
            }
            var response = new DiscordEmbedBuilder();
            response.WithColor(DiscordColor.SpringGreen);
            response.Description = $"Registered {channel.DisplayName} to play back in this channel";
            await ctx.EditResponseAsync(new DiscordWebhookBuilder().AddEmbed(response));
        }
        catch (DbUpdateException exc)
        {
            if (exc.InnerException.Message.ToUpper().Contains("UNIQUE CONSTRAINT"))
            {
                Log.Warning($"Attempted to set Apex channel {channel.ChannelId} ({channel.DisplayName}) to Discord Channel ({ctx.Channel.Id}) multiple times.");
                var response = new DiscordEmbedBuilder();
                response.WithColor(DiscordColor.IndianRed);
                response.Description = $"{channel.DisplayName} is already playing back in this channel";
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().AddEmbed(response));
            }
            else
            {
                Log.Error("DbUpdateException: " + exc.ToString());
            }
        }
    }

    [SlashCommand("stop", "Stops all playback of APEX chat to this channel.")]
    [SlashRequireUserPermissions(Permissions.ManageChannels)]
    public async Task StopPlayback(InteractionContext ctx)
    {
        using (var db = DB.ChatContext.GetNewContext())
        {
            var chanreg = db.ChannelRegistrations
                .Where(cr => cr.DiscordChannelID == ctx.Channel.Id)
                .ToList();
            if (chanreg.Count == 0)
            {
                await ctx.CreateResponseAsync("There are no APEX chats being played back in here.", true);
                return;
            }
            db.ChannelRegistrations.RemoveRange(chanreg);
            await db.SaveChangesAsync();
            await ctx.CreateResponseAsync($"Removing: {string.Join(", ", chanreg.Select(cr => cr.ApexChannelName))}", true);
        }
    }
}

