﻿using System;
using System.Linq;

using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.SlashCommands;
using DSharpPlus.SlashCommands.Attributes;
using System.Threading.Tasks;

namespace apexchat.Commands;

public class AdminSlashCommands : ApplicationCommandModule
{
    [SlashCommand("list-joined-guilds", "Display a list of the joined guilds for this bot..")]
    [SlashRequireOwner]
    public async Task ListJoinedGuilds(InteractionContext ctx)
    {
        await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource, new DiscordInteractionResponseBuilder().AsEphemeral(true));
        var guilds = ctx.Client.Guilds;
        var embed = new DiscordEmbedBuilder()
            .WithTitle("List of guilds")
            .WithColor(DiscordColor.CornflowerBlue)
            .AddField($"{guilds.Count} Guild(s)", string.Join("\n", guilds.Select(g => $"`{g.Value.Id}` {g.Value.Name}").ToList()));
        await ctx.EditResponseAsync(new DiscordWebhookBuilder().AddEmbed(embed));
    }

    [SlashCommand("leave-guild", "Leave a particular guild.")]
    [SlashRequireOwner]
    public async Task LeaveGuild(InteractionContext ctx,
        [Option("guildId", "ID number of the guild to leave")]
        string guildId)
    {
        ulong _guildId = Convert.ToUInt64(guildId);

        await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource, new DiscordInteractionResponseBuilder().AsEphemeral(true));

        var guilds = ctx.Client.Guilds;
        DiscordEmbedBuilder embed = null;
        if (guilds.Select(g => g.Value.Id).Contains(_guildId))
        {
            DiscordGuild guild = guilds.Where(g => g.Value.Id == _guildId).FirstOrDefault().Value;
            var channelList = guild.Channels.ToArray().Select(ch => ch.Value.Id);
            await guild.LeaveAsync();
            var mappedChannelCount = 0;
            using (var db = DB.ChatContext.GetNewContext())
            {
                var crs = db.ChannelRegistrations.Where(cr => channelList.Contains(cr.DiscordChannelID));
                mappedChannelCount = crs.Count();
                db.ChannelRegistrations.RemoveRange(crs);
                db.SaveChanges();
            }
            embed = new DiscordEmbedBuilder()
                .WithTitle("Success")
                .WithColor(DiscordColor.CornflowerBlue)
                .WithDescription($"Left guild {guild.Id} {guild.Name}, removed {mappedChannelCount} channel mappings");
        }
        else
        {
            embed = new DiscordEmbedBuilder()
                .WithTitle("Failure")
                .WithColor(DiscordColor.IndianRed)
                .WithDescription("Failed to leave guild: ID not found in list of guilds.");
        }
        await ctx.EditResponseAsync(new DiscordWebhookBuilder().AddEmbed(embed));
    }
}
