﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;

using DSharpPlus;
using DSharpPlus.Entities;
using Serilog;

namespace apexchat.Modules
{
    class ChatWatcher
    {
        public static bool keepRunning = true;
        private static Mutex checkingMessages = new Mutex();

        public static void FetchChatInfinitely(DiscordClient discord)
        {
            Task.Run(async () =>
            {
                while (keepRunning)
                {
                    FetchChat(discord);
                    await Task.Delay(Globals.chatCheckDelayMs);
                }
            });
        }

        private static List<string> GetApexChannelIds(DB.ChatContext db)
        {
            List<string> apexChannelIds = null;
            try
            {
                apexChannelIds = db.ChannelRegistrations
                    .Select(cr => cr.ApexChannelId)
                    .Distinct()
                    .ToList();
            }
            catch (System.InvalidOperationException)
            {
                // "Sequence contains no elements....
                Log.Warning("No channels set to be watched.");
            }
            return apexChannelIds;
        }

        private static DateTime? GetTimeForMessageUpdates(DB.ChatContext db)
        {
            try
            {
                return db.ApexDiscordMessageMaps
                    .Select(ad => ad.ApexTimestamp)
                    .Max();
            }
            catch (System.InvalidOperationException)
            {
                // "Sequence contains no elements....
                Log.Information("no chat ever recorded; finding recent.");
            }
            return null;
        }

        public static async void FetchChat(DiscordClient discord)
        {
            bool haveMutex = checkingMessages.WaitOne(1);
            if (!haveMutex)
            {
                Log.Debug("Aborting fetch");
                return;
            }
            List<FIO.Data.ChatMessage> messages = null;
            using (var db = DB.ChatContext.GetNewContext())
            {
                Log.Debug("Fetching chat....");
                List<string> apexChannelIds = GetApexChannelIds(db);
                if(apexChannelIds == null)
                {
                    checkingMessages.ReleaseMutex();
                    return;
                }
                DateTime? since = GetTimeForMessageUpdates(db);
                // query FIO
                var parameters = new Dictionary<string, string>() { };
                if (since != null)
                {
                    parameters["updated_since"] = since?.AddSeconds(1).ToString("yyyy-MM-ddTHH:mm:ss");
                    parameters["top"] = $"{Globals.maxMessageHistoryCount}";
                }
                else
                {
                    parameters["top"] = "5";
                }
                parameters["channel_ids"] = string.Join(",", apexChannelIds);
                var request = new FIO.Request("Get", "/chat/messages", parameters);
                messages = await request.GetResponseAsync<List<FIO.Data.ChatMessage>>();
            }

            if (messages != null)
            {
                Log.Debug($"{messages.Count} messages received");
                OutputMessagesToDiscord(messages, discord);
            }
            else
            {
                Log.Debug($"No messages in the result set");
            }
            checkingMessages.ReleaseMutex();
        }

        public static DiscordMessageBuilder ToDiscordMessage(FIO.Data.ChatMessage message)
        {
            var msg = new DiscordMessageBuilder();
            switch(message.MessageType)
            {
                case "DELETED":
                    msg.Content = $"***{message.UserName}** deleted this message.*";
                    break;
                case "JOINED":
                    msg.Content = $"***{message.UserName}** joined.*";
                    break;
                case "LEFT":
                    msg.Content = $"***{message.UserName}** left.*";
                    break;
                default:
                    msg.Content = $" \t**{message.UserName}**:\t{message.MessageText}";
                    break;
            }
            msg.Content = msg.Content.Substring(0, Math.Min(2000, msg.Content.Length));
            msg.WithAllowedMention(new UserMention(0));
            return msg;
        }

        public static void OutputMessagesToDiscord(List<FIO.Data.ChatMessage> messages, DiscordClient discord)
        {
            using(var db = DB.ChatContext.GetNewContext())
            {
                var apexChannelList = db.ChannelRegistrations
                    .Where(cr => messages.Select(m => m.ChatModelId).Distinct().Contains(cr.ApexChannelId))
                    .ToList();
                List<DB.Model.ApexDiscordMessageMap> existingMessages = db.ApexDiscordMessageMaps
                    .Where(admm => messages.Select(m => m.MessageId).ToList().Contains(admm.ApexMessageID))
                    .ToList();
                
                Dictionary<ulong, DSharpPlus.Entities.DiscordChannel> dchannels = new();
                var discordChannelIds = apexChannelList.Select(acl => acl.DiscordChannelID).Distinct();
                foreach(var chanid in discordChannelIds)
                {
                    try
                    {
                        var channel = discord.GetChannelAsync(chanid).Result;
                        dchannels[channel.Id] = channel;
                    }
                    catch (AggregateException exc)
                    {
                        if(exc.InnerException is DSharpPlus.Exceptions.UnauthorizedException)
                        {
                            // We don't have permissions to see this channel anymore.  Log & move on.
                            if (exc.InnerException is DSharpPlus.Exceptions.UnauthorizedException)
                            {
                                // can occur if we were prevented from sending messages to the channel via perms.
                                Log.Error($"Unable to access {chanid}.  Removing channel from registration.");
                                db.ChannelRegistrations
                                    .RemoveRange(apexChannelList.Where(cr => cr.DiscordChannelID == chanid).ToList());
                                db.SaveChanges();
                            }
                            else
                            {
                                throw;
                            }
                        }
                    }
                }

                List<DB.Model.ApexDiscordMessageMap> mappedMessages = new();
                var existingMessageIds = existingMessages.Select(em => em.ApexMessageID);
                foreach (var message in messages.OrderBy(m => m.MessageTimestamp))
                {
                    if (existingMessageIds.Contains(message.MessageId))
                    {
                        // Need to iterate through the existing mappings and update them.
                        // It's an "old" message, so don't bother replaying it to any "new"
                        // Discord channels that have registered with us.
                        var emsgs = existingMessages.Where(em => em.ApexMessageID == message.MessageId).ToList();
                        foreach(var exist in emsgs)
                        {
                            if(exist.ApexTimestamp < message.Timestamp)
                            {
                                var channel = discord.GetChannelAsync(exist.DiscordChannelID).Result;
                                var dmsg = channel.GetMessageAsync(exist.DiscordMessageID).Result;
                                dmsg.ModifyAsync(ToDiscordMessage(message));
                                exist.ApexTimestamp = message.Timestamp;
                                mappedMessages.Add(exist);
                                db.Update(exist);
                            }
                        }
                    }
                    else
                    {
                        // Play this new message through all of the registered discord channels.
                        var DiscordChannelList = apexChannelList.Where(ac => ac.ApexChannelId == message.ChatModelId).ToList();
                        foreach(var channelMap in DiscordChannelList)
                        {
                            var admm = new DB.Model.ApexDiscordMessageMap();
                            if(dchannels.ContainsKey(channelMap.DiscordChannelID))
                            {
                                var discordChannel = dchannels[channelMap.DiscordChannelID];
                                try
                                {
                                    var discordMessage = discord.SendMessageAsync(discordChannel, ToDiscordMessage(message)).Result;
                                    admm.ApexMessageID = message.MessageId;
                                    admm.ApexTimestamp = message.Timestamp;
                                    admm.DiscordChannelID = channelMap.DiscordChannelID;
                                    admm.DiscordMessageID = discordMessage.Id;
                                    mappedMessages.Add(admm);
                                }
                                catch (AggregateException exc)
                                {
                                    Log.Debug("Exception sending message: " + exc.InnerException.ToString());
                                    if (exc.InnerException is DSharpPlus.Exceptions.UnauthorizedException)
                                    {
                                        // can occur if we were prevented from sending messages to the channel via perms.
                                        Log.Error($"Unable to send message to {discordChannel.Guild.Name}/{discordChannel.Name}.  Ignoring");
                                    }
                                    else
                                    {
                                        throw;
                                    }
                                }
                            }
                            db.Add(admm);
                        }
                    }
                }
                db.SaveChanges();
            }
        }
    }
}
