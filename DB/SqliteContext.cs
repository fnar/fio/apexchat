﻿using Microsoft.EntityFrameworkCore;

namespace apexchat.DB
{
    public class SqliteContext : ChatContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            ChatContext.LogDebug(options);
            options
                .UseSqlite(Globals.ConnectionString, opts => opts.CommandTimeout(300))
                .UseSnakeCaseNamingConvention()
                .UseLazyLoadingProxies();
        }
    }
}
