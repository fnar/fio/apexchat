﻿using System;
using System.ComponentModel.DataAnnotations;

namespace apexchat.DB.Model
{
    [Microsoft.EntityFrameworkCore.Index(nameof(DiscordChannelID))]
    [Microsoft.EntityFrameworkCore.Index(nameof(DiscordChannelID), nameof(DiscordMessageID))]
    [Microsoft.EntityFrameworkCore.Index(nameof(ApexMessageID))]
    // Note on this index: I should never have the same apex message in a discord channel twice.
    [Microsoft.EntityFrameworkCore.Index(nameof(DiscordChannelID), nameof(ApexMessageID), IsUnique = true)]
    public class ApexDiscordMessageMap
    {
        [Key]
        public int Id { get; set; }
        // referencing discord messages requires the channel ID too.
        public ulong DiscordChannelID { get; set; }
        public ulong DiscordMessageID { get; set; }
        [StringLength(32)]
        public string ApexMessageID { get; set; }
        // Used to determine if we need to update a message.
        public DateTime ApexTimestamp { get; set; }
    }
}
