﻿using System;
using System.ComponentModel.DataAnnotations;

namespace apexchat.DB.Model
{
    /*
     * Mapping for what Discord channels should receive chat messages
     * from a matching APEX channel
     */
    [Microsoft.EntityFrameworkCore.Index(nameof(DiscordChannelID))]
    [Microsoft.EntityFrameworkCore.Index(nameof(ApexChannelId))]
    [Microsoft.EntityFrameworkCore.Index(nameof(DiscordChannelID), nameof(ApexChannelId), IsUnique = true)]
    public class ChannelRegistration
    {
        [Key]
        public int Id { get; set; }
        public ulong DiscordChannelID { get; set; }
        [StringLength(32)]
        public string ApexChannelId { get; set; }
        [StringLength(255)]
        public string ApexChannelName { get; set; } // for presentation purposes.
    }
}
