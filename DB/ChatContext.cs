﻿using Microsoft.EntityFrameworkCore;

using Serilog;

namespace apexchat.DB
{
    public class ChatContext : DbContext
    {
        public DbSet<Model.ChannelRegistration> ChannelRegistrations { get; set; }
        public DbSet<Model.ApexDiscordMessageMap> ApexDiscordMessageMaps { get; set; }

        public static void LogDebug(DbContextOptionsBuilder options)
        {
            if (Globals.DebugDatabase)
            {
                options.LogTo(Log.Debug, Microsoft.Extensions.Logging.LogLevel.Information);
            }
        }

        public static ChatContext GetNewContext()
        {
            switch(Globals.DatabaseType)
            {
                case DBTypes.Sqlite:
                default:
                    return new SqliteContext();
            }
        }
    }
}
