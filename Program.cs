﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using CommandLine;
using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.SlashCommands;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace apexchat
{
    class Program
    {
        private const string internalguid = "cc6709c3-5fa6-4a32-92bb-56d72bdd7176";

        static void Main(string[] args)
        {
            Globals.InitConfiguration();

            ConfigureLogger();
            ConfigureFIORequest();

            // Parse command line arguments.
            var result = CommandLine.Parser.Default
                .ParseArguments<CmdLineOptions>(args)
                .MapResult((opts) => CmdLineOptions.RunOptions(opts), errs => CmdLineOptions.HandleParseError(errs));
            if (result < 0)
            {
                Environment.Exit(result);
            }

            // Do any pending migrations, and continue.
            //if (Globals.ShouldApplyMigration)
            //{
                using (var gm = new GlobalMutex(internalguid))
                {
                    if(!gm.Acquired())
                    {
                        Log.Error("Unable to acquire global mutex before doing DB migration.  Exiting");
                        Environment.Exit(1);
                    }
                    using (var db = DB.ChatContext.GetNewContext())
                    {
                        Log.Information("Executing database migrations.");
                        db.Database.Migrate();
                        Log.Information("Database migration finished.");
                    }
                }
                //Environment.Exit(0);
            //}


            ConfigurationSection discordConfig = (ConfigurationSection)Globals.config.GetSection("Discord");

            MainAsync(discordConfig).GetAwaiter().GetResult();
        }

        static async Task MainAsync(ConfigurationSection discordConfig)
        {
            // Initialize Discord client
            var discord = new DiscordClient(new DiscordConfiguration()
            {
                Token = discordConfig["Token"],
                TokenType = TokenType.Bot,
            });

            // Initialize slash command set.
            var slashCommands = discord.UseSlashCommands();
            var debugGuildId = discordConfig.GetValue<ulong?>("DebugGuildID");
            if (debugGuildId != null && debugGuildId == 0)
                debugGuildId = null;
            if (debugGuildId != null)
                Log.Warning($"Registering slash commands to a single guild: {debugGuildId}");
            else
                Log.Debug("Registering slash commands for all guilds");
            slashCommands.RegisterCommands<Commands.AdminSlashCommands>(debugGuildId);
            slashCommands.RegisterCommands<Commands.ChatSlashCommands>(debugGuildId);

            // Fire up some async commands.
            discord.Ready += async (s, e) =>
            {
                await discord.UpdateStatusAsync(new DiscordActivity("APEX Chat", ActivityType.Watching));
            };

            await discord.ConnectAsync();

            Modules.ChatWatcher.FetchChatInfinitely(discord);

            //SendTestMessage(discord);

            await Task.Delay(-1);
        }

        static async void SendTestMessage(DiscordClient discord)
        {
            ulong channelid = 891350534127235092;
            var channel = await discord.GetChannelAsync(channelid);
            var msg = new DiscordMessageBuilder();
            msg.Content = $"Just testing https://fnar.net";
            msg.WithAllowedMention(new UserMention(discord.CurrentUser.Id));
            msg.WithEmbed(null);
            await discord.SendMessageAsync(channel, msg);
        }

        static void ConfigureLogger()
        {
            Globals.InitConfiguration();

            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(Globals.config)
                .CreateLogger();
        }

        static void ConfigureFIORequest()
        {
            Globals.InitConfiguration();
            FIO.Request.RootUrl = Globals.RootFIOUrl;
        }
    }


    public class CmdLineOptions
    {
        [CommandLine.Option('m', "migrate", Required = false, HelpText = "Run database migrations")]
        public bool ShouldApplyMigration { get; set; }

        [CommandLine.Option("debug-database", Required = false, HelpText = "Debug database operations")]
        public bool DebugDatabase { get; set; }

        // Executed when the command line options parse successfully.
        public static int RunOptions(CmdLineOptions o)
        {
            Globals.ShouldApplyMigration = o.ShouldApplyMigration;
            Globals.DebugDatabase = o.DebugDatabase;
            Log.Debug($"Options:\n\tMigrate: {Globals.ShouldApplyMigration}");
            return 0;
        }

        // Executed if a command line option has problems.
        // Also executed with `--help`?
        public static int HandleParseError(IEnumerable<Error> errs)
        {
            Log.Debug("CmdLine parse errors {0}", errs.Count());
            if (errs.Any(x => x is HelpRequestedError || x is VersionRequestedError))
                return -1;
            Log.Debug("Parse error encountered.");
            return -2;
        }
    }
}
