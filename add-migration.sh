#!/bin/sh

dotnet ef migrations add $1 --context=SqliteContext --output-dir=Migrations/SqliteMigrations
#dotnet ef migrations add $1 --context=PostgresContext --output-dir=Migrations/PostgresMigrations
#dotnet ef migrations add $1 --context=SqlServerContext --output-dir=Migrations/SqlServer
