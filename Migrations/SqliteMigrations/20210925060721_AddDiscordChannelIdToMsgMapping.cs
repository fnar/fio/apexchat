﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace apexchat.Migrations.SqliteMigrations
{
    public partial class AddDiscordChannelIdToMsgMapping : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "ix_apex_discord_message_maps_discord_message_id",
                table: "apex_discord_message_maps");

            migrationBuilder.DropIndex(
                name: "ix_apex_discord_message_maps_discord_message_id_apex_message_id",
                table: "apex_discord_message_maps");

            migrationBuilder.AddColumn<ulong>(
                name: "discord_channel_id",
                table: "apex_discord_message_maps",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0ul);

            migrationBuilder.CreateIndex(
                name: "ix_apex_discord_message_maps_discord_channel_id",
                table: "apex_discord_message_maps",
                column: "discord_channel_id");

            migrationBuilder.CreateIndex(
                name: "ix_apex_discord_message_maps_discord_channel_id_apex_message_id",
                table: "apex_discord_message_maps",
                columns: new[] { "discord_channel_id", "apex_message_id" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_apex_discord_message_maps_discord_channel_id_discord_message_id",
                table: "apex_discord_message_maps",
                columns: new[] { "discord_channel_id", "discord_message_id" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "ix_apex_discord_message_maps_discord_channel_id",
                table: "apex_discord_message_maps");

            migrationBuilder.DropIndex(
                name: "ix_apex_discord_message_maps_discord_channel_id_apex_message_id",
                table: "apex_discord_message_maps");

            migrationBuilder.DropIndex(
                name: "ix_apex_discord_message_maps_discord_channel_id_discord_message_id",
                table: "apex_discord_message_maps");

            migrationBuilder.DropColumn(
                name: "discord_channel_id",
                table: "apex_discord_message_maps");

            migrationBuilder.CreateIndex(
                name: "ix_apex_discord_message_maps_discord_message_id",
                table: "apex_discord_message_maps",
                column: "discord_message_id");

            migrationBuilder.CreateIndex(
                name: "ix_apex_discord_message_maps_discord_message_id_apex_message_id",
                table: "apex_discord_message_maps",
                columns: new[] { "discord_message_id", "apex_message_id" },
                unique: true);
        }
    }
}
