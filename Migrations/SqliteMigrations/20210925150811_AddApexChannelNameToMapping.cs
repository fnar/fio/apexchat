﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace apexchat.Migrations.SqliteMigrations
{
    public partial class AddApexChannelNameToMapping : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "apex_channel_name",
                table: "channel_registrations",
                type: "TEXT",
                maxLength: 255,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "apex_channel_name",
                table: "channel_registrations");
        }
    }
}
