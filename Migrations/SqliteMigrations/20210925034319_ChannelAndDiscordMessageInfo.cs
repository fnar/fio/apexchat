﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace apexchat.Migrations.SqliteMigrations
{
    public partial class ChannelAndDiscordMessageInfo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "apex_discord_message_maps",
                columns: table => new
                {
                    id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    discord_message_id = table.Column<ulong>(type: "INTEGER", nullable: false),
                    apex_message_id = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    apex_timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_apex_discord_message_maps", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "channel_registrations",
                columns: table => new
                {
                    id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    discord_channel_id = table.Column<ulong>(type: "INTEGER", nullable: false),
                    apex_channel_id = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_channel_registrations", x => x.id);
                });

            migrationBuilder.CreateIndex(
                name: "ix_apex_discord_message_maps_apex_message_id",
                table: "apex_discord_message_maps",
                column: "apex_message_id");

            migrationBuilder.CreateIndex(
                name: "ix_apex_discord_message_maps_discord_message_id",
                table: "apex_discord_message_maps",
                column: "discord_message_id");

            migrationBuilder.CreateIndex(
                name: "ix_apex_discord_message_maps_discord_message_id_apex_message_id",
                table: "apex_discord_message_maps",
                columns: new[] { "discord_message_id", "apex_message_id" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_channel_registrations_apex_channel_id",
                table: "channel_registrations",
                column: "apex_channel_id");

            migrationBuilder.CreateIndex(
                name: "ix_channel_registrations_discord_channel_id",
                table: "channel_registrations",
                column: "discord_channel_id");

            migrationBuilder.CreateIndex(
                name: "ix_channel_registrations_discord_channel_id_apex_channel_id",
                table: "channel_registrations",
                columns: new[] { "discord_channel_id", "apex_channel_id" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "apex_discord_message_maps");

            migrationBuilder.DropTable(
                name: "channel_registrations");
        }
    }
}
